( function( api ) {

	// Extends our custom "hottest" section.
	api.sectionConstructor['hottest'] = api.Section.extend( {

		// No events for this type of section.
		attachEvents: function () {},

		// Always make the section active.
		isContextuallyActive: function () {
			return true;
		}
	} );

} )( wp.customize );