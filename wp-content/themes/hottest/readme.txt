﻿=== Hottest ===

Contributors: StarThemes
Requires at least: WordPress 4.4  
Tested up to: WordPress 5.2
Requires PHP: 5.2.4
Stable tag: 1.1.2
Version: 1.1.2
License: GPLv2 or later  
License URI: http://www.gnu.org/licenses/gpl-2.0.html  
Tags:  food-and-drink, right-sidebar, custom-background, custom-header, custom-menu, featured-images, theme-options, threaded-comments, custom-logo, blog, full-width-template

== Description ==

Hottest is a clean, elegant and imaginative restaurant WordPress theme. This this responsive and made for restaurants, cafe, bars and other related to food industries. This theme comes with some options in customizer so you can showcase you online presence very easily. This theme design can attract the customers to explore more about your shop. This theme has been made by expert developers and designer by analyzing the food industries and their demands. This theme has comes with the easy to use customizer so you can setup your site within few clicks. Preloaded google fonts comes with this theme can make your font selection hassle free. It's fully responsive theme can performance nicely on the mobile and tablet view.
 
== Theme Resources == 

Theme is built using the following resource bundles:

* All js that have been used are within folder /js of theme.

*   jQuery Nivo Slider
	Copyright 2012, Dev7studios, under MIT license
	http://nivo.dev7studios.com
	MIT License URL: https://opensource.org/licenses/MIT

*   Assistant- https://www.google.com/fonts/specimen/Assistant
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://www.apache.org/licenses/LICENSE-2.0.html
					
		
*   hottest/customize-pro	

		Customize Pro code based on Justintadlock’s Customizer-Pro 
		Source Code URL : https://github.com/justintadlock/trt-customizer-pro			
		License : http://www.gnu.org/licenses/gpl-2.0.html
		Copyright 2016, Justin Tadlock	justintadlock.com
		
== Screenshots ==
        
*       Slider Images Are Pxhere Images:  
		
		https://pxhere.com/en/photo/35530
		
		
		Copyright 2019, Pxhere
		https://pxhere.com
		Pxhere provides images under CC0 license
 		CC0 license: https://creativecommons.org/share-your-work/public-domain/cc0
		
*  Images used
		hottest/images/mobile_nav_right.png - Self created by starthemes.
		hottest/images/slide-nav.png - Self created by starthemes.
		Images distributed under the terms of the GNU GPL
		License URI: http://www.gnu.org/licenses/gpl-2.0.html
			

*        License

	 Hottest WordPress Theme, Copyright 2019 StarThemes
	 Hottest is distributed under the terms of the GNU GPL
        
For any help you can mail us at support[at]starthemes.net

== Changelog ==

= 1.0 =
*      Initial version release.

= 1.1 =
*      Theme updated by removing warnings and unused functions.

= 1.1.1 =
*      Theme Updated by removing unused functions and few bugs.

= 1.1.2 =
*      Theme Updated by removing unused functions.

= 1.1.3 =
*      Theme Updated by prefix variable.

